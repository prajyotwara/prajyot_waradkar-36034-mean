// step 1: require express
const express = require('express')

// step 2: create express application
const app = express()

// step 3: ROUTE (mapping of http GET, url and handler (function))
// GET /
app.get('/', (request, response) => {
  console.log('GET / received')
  response.end('from GET /')
})

app.get('/products', (request, response) => {
  console.log('select * from products')
  response.end('from GET /product')
})

app.post('/products', (request, response) => {
  console.log('insert into products')
  response.end('from POST /product')
})


app.put('/products', (request, response) => {
  console.log('update products')
  response.end('from PUT /product')
})

app.delete('/products', (request, response) => {
  console.log('delete products')
  response.end('from DELETE /product')
})

// step 4: listen on port 3000
app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})

            