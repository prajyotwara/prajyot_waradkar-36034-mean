import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  firstName = 'Prajyot'
  lastName = 'Waradkar'

  person = {
    name: 'person2',
    address: 'Delhi',
    phone: '+91234234243',
    email: 'person1@test.com'
  }

  constructor() { }

  ngOnInit(): void {
  }

}
