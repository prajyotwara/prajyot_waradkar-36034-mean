const express = require('express')
const bodyParser = require('body-parser')

const studentRouter = require('./user/routes/student')
const facultyRouter = require('./user/routes/faculty')

const app = express()


app.use('/student',studentRouter)
app.use('/faculty',facultyRouter)

app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})
