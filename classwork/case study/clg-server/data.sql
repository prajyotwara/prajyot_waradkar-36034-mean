CREATE DATABASE casestudy;

USE casestudy;

CREATE TABLE admin (
    id int PRIMARY KEY AUTO_INCREMENT,
    firstname text,
    lastname text,
    address text,
    contactNo text,
    email text,
    password text
);

CREATE TABLE faculty (
    id int PRIMARY KEY AUTO_INCREMENT,
    firstname text,
    lastname text,
    address text,
    contactNo text,
    email text,
    password text,
    experience int,
    age int,
    classroomId int,
    FOREIGN KEY (classroomId) references classroom(id)
);

CREATE TABLE classroom (
    id int PRIMARY KEY AUTO_INCREMENT,
    name text,
    dateTime TIMESTAMP,
    size int,
    facultyId int,
    FOREIGN KEY (facultyId) references faculty(id)
);

CREATE TABLE student (
    id int PRIMARY KEY AUTO_INCREMENT,
    firstname text,
    lastname text,
    address text,
    contactNo text,
    email text,
    password text,
    classroomId int,
    FOREIGN KEY (classroomId) references classroom(id)
);


