const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/AllStuents', (request, response) => {
    const {classroomId} = request.params
    const statement = `select * from student`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

router.get('/:classroomId', (request, response) => {
    const {classroomId} = request.params
    const statement = `select * from student where classroomId = '${classroomId}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

router.get('/:facultyId', (request, response) => {
    const {facultyId} = request.params
    const statement = `select * from student s inner join faculty f 
                       ON s.classroomId = f.classroomId 
                       where f.facultyId = '${facultyId}' `
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })



// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/register', (request, response) => {
    const {firstname,lastname,address,contactNo, email,password} = request.body

    const statement =`insert into student (firstname,lastname,address,contactNo, email,password) values (
        '${firstname}', '${lastname}','${address}', '${contactNo}', '${email}', '${crypto.SHA256(password)}',
      )`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})

router.post('', (request, response) => {
  response.send()
})
// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('', (request, response) => {
  response.send()
})

// ----------------------------------------------------


// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from student where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
// ----------------------------------------------------

module.exports = router