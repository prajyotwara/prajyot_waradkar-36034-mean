const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const crypto = require('crypto-js')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/AllFaculty', (request, response) => {
    const statement = `select * from faculty`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('', (request, response) => {
  response.send()
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('', (request, response) => {
  response.send()
})

// ----------------------------------------------------


// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from faculty where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
// ----------------------------------------------------

module.exports = router