const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const crypto = require('crypto-js')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('', (request, response) => {
  response.send()
})

// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/register', (request, response) => {
    const {firstname,lastname,address,contactNo,email,password} = request.body
    const statement =`insert into admin (firstname,lastname,address,contactNo, email,password) values (
        '${firstname}', '${lastname}','${address}', '${contactNo}', '${email}', '${crypto.SHA256(password)}'
      )`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})

router.post('/login', (request, response) => {
    const {email, password} = request.body
    const statement = `select id, firstname, lastname from admin where email = '${email}' and password = '${crypto.SHA256(password)}'`
    db.query(statement, (error, admins) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (admins.length == 0) {
          response.send({status: 'error', error: 'admin does not exist'})
        } else {
          const admin = admins[0]
          response.send(utils.createResult(error, {
            firstName: admin['firstname'],
            lastName: admin['lastname']
          }))
        }
      }
    })
  })


// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/:id', (request, response) => {
    const {id} = request.params
    const {firstname,lastname,address,contactNo, email} = request.body
    const statement = `update admin set firstname = '${firstname}', lastname = '${lastname}', address = '${address}', contactNo = '${contactNo}', email = '${email}' where id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

// ----------------------------------------------------


// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('', (request, response) => {
  response.send()
})

// ----------------------------------------------------

module.exports = router