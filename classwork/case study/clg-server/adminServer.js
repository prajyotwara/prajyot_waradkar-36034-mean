const express = require('express')
const bodyParser = require('body-parser')

const adminRouter = require('./admin/routes/admin')
const classroomRouter = require('./admin/routes/classroom')
const facultyRouter = require('./user/routes/faculty')
const studentRouter = require('./user/routes/student')

const app = express()

app.use(bodyParser.json())

app.use('/admin',adminRouter)
app.use('/classroom',classroomRouter)
app.use('/faculty',facultyRouter)
app.use('/student',studentRouter)

app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(3500, '0.0.0.0', () => {
  console.log('server started on port 3500')
})
