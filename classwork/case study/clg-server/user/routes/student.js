const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const crypto = require('crypto-js')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/:classroomId', (request, response) => {
    const {classroomId} = request.params
    const statement = `select * from classroom where id = ${classroomId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

router.get('/faculty/:classroomId', (request, response) => {
    const {classroomId} = request.params
    const statement = `select * from faculty f inner join classroom c 
                       on f.id= c.facultyId 
                       where c.id = ${classroomId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/register', (request, response) => {
    const {firstname,lastname,address,contactNo, email,password} = request.body

    const statement =`insert into student (firstname,lastname,address,contactNo, email,password) values (
        '${firstname}', '${lastname}','${address}', '${contactNo}', '${email}', '${crypto.SHA256(password)}',
      )`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})

router.post('/login', (request, response) => {
    const {email, password} = request.body
    const statement = `select id, firstname, lastname from student where email = '${email}' and password = '${crypto.SHA256(password)}'`
    db.query(statement, (error, students) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (faculties.length == 0) {
          response.send({status: 'error', error: 'student does not exist'})
        } else {
          const student = students[0]
          response.send(utils.createResult(error, {
            firstName: student['firstname'],
            lastName: student['lastname']
          }))
        }
      }
    })
  })
// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/:id', (request, response) => {
    const {id} = request.params
    const {firstname,lastname,address,contactNo, email} = request.body
    const statement = `update student set firstname = '${firstname}', lastname = '${lastname}', address = '${address}', contactNo = '${contactNo}', email = '${email}' where id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

// ----------------------------------------------------


// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('', (request, response) => {
  response.send()
})

// ----------------------------------------------------

module.exports = router