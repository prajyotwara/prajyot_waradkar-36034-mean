const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const crypto = require('crypto-js')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/', (request, response) => {
    const statement = `select * from faculty`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


router.get('/:classroomId', (request, response) => {
    const {classroomId} = request.params
    const statement = `select * from classroom where id = ${classroomId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

  router.get('/:classroomId/count', (request, response) => {
    const {classroomId} = request.params
    const statement = `select size from classroom where id = ${classroomId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/register', (request, response) => {
    const {firstname,lastname,address,contactNo, email,password,experience,age} = request.body

    const statement =`insert into faculty (firstname,lastname,address,contactNo, email,password,experience,age) values (
        '${firstname}', '${lastname}','${address}', '${contactNo}', '${email}', '${crypto.SHA256(password)}', '${experience}', '${age}',
      )`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
})

router.post('/login', (request, response) => {
  const {email, password} = request.body
  const statement = `select id, firstname, lastname from faculty where email = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, faculties) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (faculties.length == 0) {
        response.send({status: 'error', error: 'faculty does not exist'})
      } else {
        const faculty = faculties[0]
        response.send(utils.createResult(error, {
          firstName: faculty['firstname'],
          lastName: faculty['lastname']
        }))
      }
    }
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/:id', (request, response) => {
    const {id} = request.params
    const {firstname,lastname,address,contactNo, email,experience,age} = request.body
    const statement = `update faculty set firstname = '${firstname}', lastname = '${lastname}', address = '${address}', contactNo = '${contactNo}', email = '${email}', experience = '${experience}', age = '${age}' where id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

// ----------------------------------------------------


// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('', (request, response) => {
  response.send()
})

// ----------------------------------------------------

module.exports = router