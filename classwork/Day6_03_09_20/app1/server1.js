const express = require('express')
const { response } = require('express')

const app = express()

//middleware
function log(request,response,next){
    console.log('inside the log function')
    console.log(`method: ${request.method}`)
    console.log(`url:${request.url}`)
    next()
}

app.use(log)

app.get('/',(request, response)=> {
    console.log('Inside GET/')
    response.end('Welcome to my REST application')
})

app.post('/', (request, response) => {
    console.log('inside POST /')
    response.end('this is a post request')
})

app.put('/', (request, response) => {
    console.log('inside PUT /')
    response.end('this is a put request')
  })

app.delete('/', (request, response) => {
  console.log('inside DELETE /')
  response.end('this is a delete request')
})

// ------------------------------------
// -----  product related routes ------
// ------------------------------------

app.get('/product', (request, response) => {
  console.log('inside GET /product')
  response.end('this is GET /product')
})

app.post('/product', (request, response) => {
  console.log('inside POST /product')
  response.end('this is POST /product')
})

app.put('/product', (request, response) => {
  console.log('inside PUT /product')
  response.end('this is PUT /product')
})

app.delete('/product', (request, response) => {
  console.log('inside DELETE /product')
  response.end('this is DELETE /product')
})


app.listen(3000, '0.0.0.0', () => {
  console.log('server started successfully on port 3000')
})