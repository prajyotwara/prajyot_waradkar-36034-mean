const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category:
 *   get:
 *     description: For getting all the categories
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: successful message
 */
router.get('/', (request, response) => {
  const statement = `select id, title, description from category`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category/:
 *   post:
 *     description: For adding a product category
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of the category
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description about the category
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.post('/', (request, response) => {
  const {title, description} = request.body
  const statement = `insert into category (title, description) values ('${title}', '${description}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category/:id:
 *   put:
 *     description: For updating a product category
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of the category
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description about the category
 *         in: formData
 *         required: true
 *         type: string
 *       - name: id
 *         description: id of the category
 *         in: formData
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: successful message
 */
router.put('/:id', (request, response) => {
  const {id} = request.params
  const {title, description} = request.body
  const statement = `update category set title = '${title}', description = '${description}' where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category/:id:
 *   delete:
 *     description: For deleting a product category
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of the category
 *         in: formData
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: successful message
 */
router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from category where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router