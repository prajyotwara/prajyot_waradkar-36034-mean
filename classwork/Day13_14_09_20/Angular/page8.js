// variables
// let firstName = "person1"
// let age = 40
// function
// function printInfo() {
// }
var Person = /** @class */ (function () {
    function Person() {
    }
    // method
    Person.prototype.printInfo = function () {
        console.log("first name: " + this.firstName);
        console.log("age: " + this.age);
        console.log("address: " + this.address);
    };
    return Person;
}());
var p1 = new Person();
console.log(p1);
p1.firstName = "steve";
p1.age = 58;
p1.address = "USA";
// console.log(p1)
p1.printInfo();
// function Person(firstName, age, address) {
//   this.firstName = firstName
//   this.age = age
//   this.address = address
// }
// const p1 = new Person('person1', 40, 'pune')
