// int num = 100;
var num = 100;
console.log("num = " + num + ", type = " + typeof (num));
// okay
// num = 100
// not okay
// num = "str"
// string
var firstName = "steve";
console.log("firstName = " + firstName + ", type = " + typeof (firstName));
// string
var lastName = 'Jobs';
console.log("lastName = " + lastName + ", type = " + typeof (lastName));
// string
var address = "\n  address line,\n  city,\n  state\n";
console.log("address = " + address + ", type = " + typeof (address));
// boolean
var canVote = false;
console.log("canVote = " + canVote + ", type = " + typeof (canVote));
// undefined
var myVar;
console.log("myVar = " + myVar + ", type = " + typeof (myVar));
// myVar = 100
// object
var person = { name: "person1", age: 40, address: "pune" };
console.log("person = " + person + ", type = " + typeof (person));
// object
var mobile = { model: "iPhone XS Max" };
console.log("mobile = " + mobile + ", type = " + typeof (mobile));
var result;
result = 100;
result = "error";
result = true;
var myvar2;
myvar2 = 100;
myvar2 = "teset";
myvar2 = true;
myvar2 = { name: "test" };
myvar2 = undefined;
