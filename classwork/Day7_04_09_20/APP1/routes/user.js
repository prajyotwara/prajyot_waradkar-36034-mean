const express = require('express')

// router is used to handle the routes
const router = express.Router()

router.get('/', (request, response) => {
  console.log('inside GET /user')
  response.send('GET /user')
})

router.post('/signin', (request, response) => {
  console.log('inside POST /user/signin')
  response.send('POST /user')
})

router.post('/signup', (request, response) => {
  console.log('inside POST /user/signup')
  response.send('POST /user')
})

// the exported router can be imported and added into the server
module.exports = router