var Animal = /** @class */ (function () {
    function Animal() {
    }
    Animal.prototype.run = function () {
        console.log('animal is running');
    };
    return Animal;
}());
//service provider
var Rectangle = /** @class */ (function () {
    function Rectangle() {
    }
    Rectangle.prototype.draw = function () {
        console.log('drawing rectangle');
    };
    Rectangle.prototype.erase = function () {
        console.log('erasing rectangle');
    };
    Rectangle.prototype.rectangle = function () {
        console.log('inside rectangle');
    };
    return Rectangle;
}());
var Circle = /** @class */ (function () {
    function Circle() {
    }
    Circle.prototype.draw = function () {
        console.log('drawing circle');
    };
    Circle.prototype.erase = function () {
        console.log('erasing circle');
    };
    return Circle;
}());
var Square = /** @class */ (function () {
    function Square() {
    }
    Square.prototype.draw = function () {
        console.log('drawing square');
    };
    Square.prototype.erase = function () {
        console.log('erasing square');
    };
    return Square;
}());
function function1(drawable) {
    drawable.draw();
    drawable.erase();
}
//type of reference = type of object
var drawable1 = new Rectangle();
function1(drawable1);
var drawable2 = new Circle();
function1(drawable2);
var drawable3 = new Square();
function1(drawable3);
