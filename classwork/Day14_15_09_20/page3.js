var Person = /** @class */ (function () {
    function Person() {
    }
    // setter
    Person.prototype.setName = function (name) {
        this.name = name;
    };
    Object.defineProperty(Person.prototype, "age", {
        // getter method can be accessed as a property
        get: function () {
            return this._age;
        },
        // setter method can be accessed as a property
        set: function (age) {
            this._age = age;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "Address", {
        get: function () {
            return this.address;
        },
        set: function (address) {
            this.address = address;
        },
        enumerable: false,
        configurable: true
    });
    // getter
    Person.prototype.getName = function () {
        return this.name;
    };
    return Person;
}());
var p1 = new Person();
p1.setName("person1");
// setter property
p1.age = 20;
p1.Address = "pune";
console.log("name = " + p1.getName());
console.log("age = " + p1.age);
console.log("address = " + p1.Address);
console.log(p1);
