//step1: GET THE HTTP MODULE
const http = require('http')

//step2: CREATE A HTTP SERVER
const server = http.createServer((request,response)=> {
    console.log('a request recieved')

    //SEND THE RESPONSE
    response.end("<h1>Hey This is My Website<h1>")
})

//step3: START THE SERVER
server.listen(3000,'0.0.0.0',() => {
    console.log('server started successfully')
})