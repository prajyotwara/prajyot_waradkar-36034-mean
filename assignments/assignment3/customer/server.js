const express = require('express')
const bodyParser = require('body-parser')

// routes
const routerCustomer = require('./routes/customer')

const app = express()

// get input from user using request.body
app.use(bodyParser.json())

// add routes to the application
app.use('/customer', routerCustomer)


app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})