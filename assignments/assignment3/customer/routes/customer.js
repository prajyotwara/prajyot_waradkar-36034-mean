const express = require('express')
const db = require('../db')
const utils = require('../utilis')

const router = express.Router()

router.get('/', (request, response) => {

    // response.send()
    // const Name = request.body.Name
    // const Password = request.body.Password
    // const Mobile = request.body.Mobile
    // const Address = request.body.Address
    // const Email = request.body.Email

    const statement = `select ID,Name,Password,Mobile,Address,Email from Customer`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})


router.post('/', (request, response) => {
  
  const statement = `insert into Customer 
    (Name,Password,Mobile,Address,Email) 
     values ('${request.body.Name}', 
     '${request.body.Password}', 
     '${request.body.Mobile}', 
     '${request.body.Address}', 
     '${request.body.Email}')`

  db.query(statement, (error, dbResult) => {
    response.send(utils.createResult(error, dbResult))
    })
    
  })

  router.put('/', (request, response) => {
    
    const statement = `update Customer 
          set Name = '${request.body.Name}',
          Password = '${request.body.Password}',
          Mobile = '${request.body.Mobile}',
          Address = '${request.body.Address}',
          Email = '${request.body.Email}'
        where ID = ${request.body.ID}`

    db.query(statement, (error, dbResult) => {
      response.send(utils.createResult(error, dbResult))
    })
  
  })

  router.delete('/', (request, response) => {
    
    const statement = `delete from Customer where ID = ${request.body.ID}`
  
    db.query(statement, (error, dbResult) => {
      response.send(utils.createResult(error, dbResult))
    })
    
  })


  
  module.exports = router