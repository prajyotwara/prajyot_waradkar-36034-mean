const express = require('express')
const db = require('../db')
const utils = require('../utilis')

const router = express.Router()

router.get('/', (request, response) => {


    const statement = `select ID,Name,Type,Category,Description from PIZZA_ITEMS`
     db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})

router.post('/', (request, response) => {
  
    const statement = `insert into PIZZA_ITEMS 
    (Name,Type,Category,Description) 
     values ('${request.body.Name}', 
     '${request.body.Type}', 
     '${request.body.Category}', 
     '${request.body.Description}')`

  db.query(statement, (error, dbResult) => {
    response.send(utils.createResult(error, dbResult))
    })

    
})

router.put('/', (request, response) => {
    
    const statement = `update PIZZA_ITEMS 
          set Name = '${request.body.Name}',
          Type = '${request.body.Type}',
          Category = '${request.body.Category}',
          Description = '${request.body.Description}'
        where ID = ${request.body.ID}`
  
    db.query(statement, (error, dbResult) => {
      response.send(utils.createResult(error, dbResult))
    })
  
})

router.delete('/', (request, response) => {
    
    const statement = `delete from PIZZA_ITEMS where ID = ${request.body.ID}`
  
    db.query(statement, (error, dbResult) => {
      response.send(utils.createResult(error, dbResult))
    })
    
})

module.exports = router