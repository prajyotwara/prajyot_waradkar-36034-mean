const express = require('express')
const bodyParser = require('body-parser')

// routes
const routerPizza = require('./routes/pizza')

const app = express()

// get input from user using request.body
app.use(bodyParser.json())

// add routes to the application
app.use('/pizza', routerPizza)

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})
