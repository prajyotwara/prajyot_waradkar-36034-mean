const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')


// routers
const userRouter = require('./routes/user')
const bookRouter = require('./routes/book')


const app = express()
app.use(bodyParser.json())
app.use(morgan('combined'))

// add the routes
app.use('/user', userRouter)
app.use('/book', bookRouter)

// default route
app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})