const express = require('express')
const utils = require('../utils')
const db = require('../db')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/allBooks', (request, response) => {
    const statement = `select id,name,author,subject,price from books`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})

router.get('/byId', (request, response) => {
    const statement = `select id,name,author,subject,price from books where id =${request.body.id}`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})

router.get('/bySubject', (request, response) => {
    const subject = request.headers['subject']
    const statement = `select id,name,author,subject,price from books where subject ='${subject}'`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})

router.get('/Subject', (request, response) => {
    const statement = `select DISTINCT subject from books`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})


// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/', (request, response) => {
    const statement = `insert into books 
    (id,name,author,subject,price) 
     values ('${request.body.id}',
     '${request.body.name}', 
     '${request.body.author}', 
     '${request.body.subject}', 
     '${request.body.price}')`

  db.query(statement, (error, dbResult) => {
    response.send(utils.createResult(error, dbResult))
    })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/', (request, response) => {
    const statement = `update books 
    set 
    name = '${request.body.name}',
    author = '${request.body.author}',
    subject = '${request.body.subject}',
    price = '${request.body.price}'
  where id = ${request.body.id}`

db.query(statement, (error, dbResult) => {
response.send(utils.createResult(error, dbResult))
})
})

// ----------------------------------------------------


// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/', (request, response) => {
    
    const statement = `delete from books where id = ${request.body.id}`
  
    db.query(statement, (error, dbResult) => {
      response.send(utils.createResult(error, dbResult))
    })
    
  })
// ----------------------------------------------------

module.exports = router