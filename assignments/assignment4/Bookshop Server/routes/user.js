const express = require('express')
const utils = require('../utils')
const db = require('../db')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/allUsers', (request, response) => {
    const statement = `select id,name,password,mobile,address,email,birth from customers`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})

router.get('/byId', (request, response) => {
    const statement = `select id,name,password,mobile,address,email,birth from customers where id =${request.body.id}`
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
      })
})


// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------
router.post('/signin', (request, response) => {
    const email = request.body.email
    const password = request.body.password
    
    const statement = `select * from customers where email = '${email}' and password = '${password}'`
    db.query(statement, (error, customers) => {
      const result = { status: '' }
      console.log(customers)
      if (customers.length == 0) {
        result['status'] = 'error'
        result['error'] = 'customer does not exist'
      } else {
        const user = customers[0]
        result['status'] = 'success'
        result['data'] = {
          id: user['id'],
          email: user['email'],
          firstName: user['firstName'],
          lastName: user['lastName']
        }
      }
      response.send(result)
    })
  })
  

  
router.post('/addUser', (request, response) => {
    const statement = `insert into customers 
    (name,password,mobile,address,email,birth) 
     values ('${request.body.name}', 
     '${request.body.password}', 
     '${request.body.mobile}', 
     '${request.body.address}', 
     '${request.body.email}',
     '${request.body.birth}')`

  db.query(statement, (error, dbResult) => {
    response.send(utils.createResult(error, dbResult))
    })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/', (request, response) => {
    const statement = `update customers 
    set name = '${request.body.name}',
    password = '${request.body.password}',
    mobile = '${request.body.mobile}',
    address = '${request.body.address}',
    email = '${request.body.email}',
    birth = '${request.body.birth}'
  where id = ${request.body.id}`

db.query(statement, (error, dbResult) => {
response.send(utils.createResult(error, dbResult))
})
})

// ----------------------------------------------------


// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/', (request, response) => {
    
    const statement = `delete from customers where id = ${request.body.id}`
  
    db.query(statement, (error, dbResult) => {
      response.send(utils.createResult(error, dbResult))
    })
    
  })

// ----------------------------------------------------

module.exports = router