// require the express package
const express = require('express')

// used to parse the request body contents
const bodyParser = require('body-parser')

// require mysql for db connection
const mysql = require('mysql')

// create a server instance
const app = express()

// use body parser's json parser to parse body into json object
app.use(bodyParser.json())

// ------------------------------------------
// -------------PizzaDetails routes -------------
// ------------------------------------------


app.get('/pizza', (request, response) => {

    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    // prepare the statement
    const statement = `select ID,Name,Type,Category,Description from PIZZA_ITEMS`
      
    // execute the statement
    connection.query(statement, (error, data) => {
  
      // close the connection
      connection.end()
  
      // if any error ocurrs while executing query
      if (error) {
        response.end('error while executing query')
      } else {
  
        
        response.send(data)
  
      }
    })
  
  })

  app.post('/pizza', (request, response) => {
    console.log(`body: `)
    console.log(request.body)
  
    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    const statement = `insert into PIZZA_ITEMS 
               (Name,Type,Category,Description) 
                values ('${request.body.Name}', 
                '${request.body.Type}', 
                '${request.body.Category}', 
                '${request.body.Description}')`
  
    // console.log(statement)
    connection.query(statement, (error, result) => {
      // close the connection
      connection.end()
      if(error){
        console.log(error)
      }else{
        console.log('Query executed successfully')
        response.send(result)
    }
    })
  })

  app.put('/pizza', (request, response) => {
    console.log(`body: `)
    console.log(request.body)
  
    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    const statement = `update PIZZA_ITEMS 
          set Name = '${request.body.Name}',
          Type = '${request.body.Type}',
          Category = '${request.body.Category}',
          Description = '${request.body.Description}'
        where ID = ${request.body.ID}`
  
    // console.log(statement)
    connection.query(statement, (error, result) => {
      // close the connection
      connection.end()
      if(error){
          console.log(error)
      }else{
          console.log('Query executed successfully')
          response.send(result)
      }
    })
  })
  
  app.delete('/pizza', (request, response) => {
    console.log(`body: `)
    console.log(request.body)
  
    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    const statement = `delete from PIZZA_ITEMS where ID = ${request.body.ID}`
  
    // console.log(statement)
    connection.query(statement, (error, result) => {
      // close the connection
      connection.end()
      if(error){
        console.log(error)
      }else{
        console.log('Query executed successfully')
        response.send(result)
    }
    })
  })
  
  // ------------------------------------------
  // -------------- user routes ---------------
  // ------------------------------------------

  // listen on port 3000 (start the server)
  app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
  })