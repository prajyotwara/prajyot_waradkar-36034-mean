// require the express package
const express = require('express')

// used to parse the request body contents
const bodyParser = require('body-parser')

// require mysql for db connection
const mysql = require('mysql')

// create a server instance
const app = express()

// use body parser's json parser to parse body into json object
app.use(bodyParser.json())

// ------------------------------------------
// -------------Customer routes -------------
// ------------------------------------------


app.get('/customer', (request, response) => {

    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    // prepare the statement
    const statement = `select ID,Name,Password,Mobile,Address,Email from Customer`

    // execute the statement
    connection.query(statement, (error, data) => {
  
      // close the connection
      connection.end()
  
      // if any error ocurrs while executing query
      if (error) {
        response.end('error while executing query')
      } else {
  
        
        response.send(data)
  
      }
    })
  
  })
  

  app.post('/customer', (request, response) => {
    console.log(`body: `)
    console.log(request.body)
  
    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    const statement = `insert into Customer 
               (Name,Password,Mobile,Address,Email) 
                values ('${request.body.Name}', 
                '${request.body.Password}', 
                '${request.body.Mobile}', 
                '${request.body.Address}', 
                '${request.body.Email}')`
  
    // console.log(statement)
    connection.query(statement, (error, result) => {
      // close the connection
      connection.end()
      if(error){
        console.log(error)
      }else{
        console.log('Query executed successfully')
        response.send(result)
    }
    })
  })
  
  app.put('/customer', (request, response) => {
    console.log(`body: `)
    console.log(request.body)
  
    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    const statement = `update Customer 
          set Name = '${request.body.Name}',
          Password = '${request.body.Password}',
          Mobile = '${request.body.Mobile}',
          Address = '${request.body.Address}',
          Email = '${request.body.Email}'
        where ID = ${request.body.ID}`
  
    // console.log(statement)
    connection.query(statement, (error, result) => {
      // close the connection
      connection.end()
      if(error){
          console.log(error)
      }else{
          console.log('Query executed successfully')
          response.send(result)
      }
    })
  })
  
  app.delete('/customer', (request, response) => {
    console.log(`body: `)
    console.log(request.body)
  
    // creating a mysql connection
    const connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: 'manager',
      database: 'mydb'
    })
  
    // opening the connection
    connection.connect()
  
    const statement = `delete from Customer where ID = ${request.body.ID}`
  
    // console.log(statement)
    connection.query(statement, (error, result) => {
      // close the connection
      connection.end()
      if(error){
        console.log(error)
      }else{
        console.log('Query executed successfully')
        response.send(result)
    }
    })
  })
  
  // ------------------------------------------
  // -------------- user routes ---------------
  // ------------------------------------------

  // listen on port 3000 (start the server)
  app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
  })